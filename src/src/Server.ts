import cookieParser from 'cookie-parser';
import express from 'express';
import e, { Request, Response } from 'express';
import logger from 'morgan';
import path from 'path';
import BaseRouter from './routes';
import fs from "fs"
import bodyParser from "body-parser"
import { BlobServiceClient, StorageSharedKeyCredential } from "@azure/storage-blob"
//@ts-ignore
import fetch from "node-fetch";
import { Readable } from 'stream';

// Init express
const app = express();
app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: true,
  parameterLimit: 100000000
}));
app.use(bodyParser.json({ limit: '50mb', strict: false }));
// Add middleware/settings/routes to express.
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', BaseRouter);

/**
 * Point express to the 'views' directory. If you're using a
 * single-page-application framework like react or angular
 * which has its own development server, you might want to
 * configure this to only serve the index file while in
 * production mode.
 */
const viewsDir = path.join(__dirname, 'views');
app.set('views', viewsDir);

//Always HTTPS
//app.use((req, res, next) => {
//    const proto = req.headers["x-forwarded-proto"] || "";
//    if(!req.headers.host!.includes("localhost") && proto !== "https"){
//        const secureUrl = "https://" + req.headers.host + (req.url === "/" ? "/editor" : req.url);
//        res.redirect(secureUrl);
//    }
//    else {
//        next();
//    }
//})


const b64ToArrayBuff = (base64: string) => {
  const bynString = window.atob(base64);
  var bynLen = bynString.length;
  var bytes = new Uint8Array(bynLen);
  for(let i = 0; i < bynLen; i++){
    var ascii = bynString.charCodeAt(i);
    bytes[i] = ascii;
  }
  return bytes;
}

app.post("/uploadImage", async (req, res) => {
  const format = req.body.split(',')[0].match(new RegExp("\/.*?;"))![0].slice(1, -1);
  const base64 = req.body.split(',')[1];
  const blobName = "blob-" + Math.random().toString(36).substring(7) + "-" + new Date().toISOString() + "." + (format? format : "jpg");
  console.log(blobName)

  const sharedK = new StorageSharedKeyCredential(
    "s9eventlicstoredevv2", 
    "k2Liigen5aJ6qFXNp5p46ou1iE7Dm8+h2PVd1cIwDegPAUcfp2TSWVQ/wuOZh5ShjOZgW4MXwpzk2hz6wM5yVQ=="
  );
  const blobServiceClient = new BlobServiceClient(
    "https://s9eventlicstoredevv2.blob.core.windows.net",
    sharedK
  );
  const containerClient = await blobServiceClient.getContainerClient("pdf-templar");
  const blockClient = await containerClient.getBlockBlobClient(blobName);
  const buffer = Buffer.from(base64, "base64");


  const rs = await blockClient.upload(buffer, buffer.length)
  res.status(200).end(`https://s9eventlicstoredevv2.blob.core.windows.net/pdf-templar/${blobName}`);
})

app.get('/healthcheck', (req, res) => {
  res.status(200).end("{}");
})

app.get('/', (req, res) => {
  let redirectString = `/editor`

  if (req.query) {
    if (req.query.data_endpoint)
      redirectString += `?data_endpoint=${req.query.data_endpoint}`
    if (req.query.post_url)
      redirectString += `&post_url=${req.query.post_url}`
  }
  redirectString = (req.headers.host!.includes("localhost") ? "http://" : "https://") + req.headers.host + redirectString;
  res.redirect(redirectString);
})
app.use(express.static(path.join(__dirname, 'editor/')));
app.use("/editor", express.static(path.join(__dirname, 'editor/')));
app.use("/guide", express.static(path.join(viewsDir + "/userGuide")));
//app.get('/', serveStatic)


app.on('unhandledRejection', (parent) => {
  console.log(parent.stack);
})

// Export express instance
export default app;
