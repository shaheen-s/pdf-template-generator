import canvas, { Canvas, Image, createCanvas, createImageData, loadImage } from "canvas"
import JsBarcode from "jsbarcode"
//@ts-ignore
import qrcode from "qrcode";

export const deepCopyObj = (obj: any) => {
    if(obj === undefined) return;
    const newObj = JSON.parse(JSON.stringify(obj));
    return newObj;
}

export const createBarcode = (value: string, format?: string) => {
  const baseBarcode = createCanvas(100, 100);
  value = value? value : "EMPTY_BARCODE";
  JsBarcode(baseBarcode, value, {format: format? format : "CODE128"});
  const barcode64 = baseBarcode.toDataURL("image/jpeg").split(",")[1]
  return barcode64;
}

export const createQrCode = async (value: string, url?: string) => {
  let code = undefined;
  await qrcode.toDataURL(value).then((url: string) => code = url);
  return code;
}

export const getPdfFontsMappings= (font: string) => {
  const fontMaps = {
    normal: "",
    italic: "",
    bold: "",
    isStandard: false
  }
  switch(font) {
    case "Cairo":
        fontMaps.normal =  "Regular",
        fontMaps.italic =  "Regular",
        fontMaps.bold   =  "Bold"
        break;
    case "Times":
        fontMaps.normal =  "Roman",
        fontMaps.italic =  "Italic",
        fontMaps.bold   =  "Bold"
        break;
    case "Helvetica":
    case "Roboto":
    case "Courier":
        fontMaps.normal =  "",
        fontMaps.italic =  "Oblique",
        fontMaps.bold   =  "Bold"
        break;
  }
  switch(font){
    case "Times":
    case "Helvetica":
    case "Courier":
      fontMaps.isStandard = true;
      break;
    default:
      fontMaps.isStandard = false;
  }
  return fontMaps;
}